<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {
	return view('welcome');
});

Route::get('/template', function () {
	return view('template');
});

Route::get('/display', 'MainController@display');
Route::post('/hello', 'MainController@hello');

Route::get('/sell', 'SellController@sell')->middleware('auth');
Route::post('/sell', 'SellController@form')->middleware('auth');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'Auth\LoginController@logout');
