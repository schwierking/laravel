<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use DB;

class CheckAPI implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $value = str_replace('-','',$value);
				$api = "https://www.googleapis.com/books/v1/volumes?q=isbn:".$value;
				$file = file_get_contents($api);
				$readable = json_decode($file);
				if ($readable -> totalItems === 0) { // not found in api
					return false;
				} else {
					$title = $readable -> items[0] -> volumeInfo -> title;
					$publisher = $readable -> items[0] -> volumeInfo -> publisher;
					$date = $readable -> items[0] -> volumeInfo -> publishedDate;
					$author_array = $readable -> items[0] -> volumeInfo -> authors;
					$authors = implode(',',$author_array);

					DB::insert("insert into book (isbn,title,authors,publisher,year) values (?,?,?,?,?)",[$value,$title,$authors,$publisher,$date]);
					return true;
				}
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid ISBN number.';
    }
}
