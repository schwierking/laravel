<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class CheckDigit implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $value = str_replace('-','',$value);
				$sum = 0;
				$check = '';

				if ($value[9] === 'x' or $value[9] === 'X') {
					for ($i = 2, $j = 8; $i < 10; $i++, $j--) {
						$sum = $sum + ($value[$j] * $i);
					}
					$sum = $sum + 10;
    		} else {
					for ($i = 1, $j = 9; $i < 10; $i++, $j--) { $sum = $sum + ($value[$j] * $i); }
				}
				$check = $sum %11;
				if ($check != 0) { return false; } 
				else { return true; }
		}

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The check-digit is invalid.';
    }
}
