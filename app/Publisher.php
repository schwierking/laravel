<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Publisher extends Model {
	protected $timestamps = false;
	protected $table = 'publisher';
	protected $primarykey = 'publisher_id';
}
