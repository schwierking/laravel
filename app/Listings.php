<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Listings extends Model {
	protected $timestamps = false;
	protected $table = 'listings';
	protected $primarykey = 'listing_id';
}
