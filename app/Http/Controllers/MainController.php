<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Listings;
use DB;

class MainController extends Controller {
	public function display() {
		$list = DB::table('listings')->get();
		return view('display',['lists' => $list]);
	}
}
