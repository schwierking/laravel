<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rules\isbnRegex;
use App\Rules\CheckDigit;
use App\Rules\CheckDB;
use App\Rules\CheckAPI;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use DB;

class SellController extends Controller {
	public function sell() {
		return view('sell');
	}
	
	public function form(Request $request) {
		$data = $request->validate([
			'isbn' => array(
				'required',
				'numeric',
				new isbnRegex(Input::get('isbn')),
				new CheckDigit(Input::get('isbn'))
			),
			'price' => array(
				'required',
				'regex:/^\d+(?:\.\d{2})?$/u',
				'between:0,200'
			)
		]);
		
		 if (!(DB::table('listing')->where('isbn',Input::get('isbn')))) {
		 	return view('sell');
		 } else {
		 	$data = $request->validate([
		 		'isbn' => new CheckAPI(Input::get('isbn'))
			]);
		 
		 }
		
		$isbn = str_replace('-','',Input::get('isbn'));
		$price = Input::get('price');
		$condition = Input::get('condition');
		$comments = Input::get('comments');
		$user = Auth::id();
		DB::insert("insert into listing (owner_id,isbn,price,condition,seller_notes) values (?,?,?,?,?)",[$user,$isbn,$price,$condition,$comments]);
		
		return view('sell2');
	}
}
