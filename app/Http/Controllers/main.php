<?php
	namespace App\Http\Controllers;

	use App\Http\Controllers\Controller;
	use App\Listings;
	use Illuminate\Support\Facades\Input;
	use DB;
	
	class MainController extends Controller {
		public function display() {
			$list = DB::table('listings')->get();
			return view('display',['lists' => $list]);
		}

		public function hello() {
			$result = DB::select('SELECT * FROM listings WHERE listing_id=?',[Input::get('listing_id')]);
			$data = json_encode($result);
			return $data;

			//$row = $_POST['listing_id'];
			//echo "Row: $row";
		}
	}
?>
