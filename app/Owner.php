<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Owner extends Authenticatable
{
	protected $table = 'owner';
	protected $primaryKey = 'owner_id';
	public $timestamps = false;

	protected $fillable = [
		'email','password',
	]; 
	
	protected $hidden = [
		'password',
	];

	public function setAttribute($key, $value) {
		$isRememberTokenAttribute = $key == $this->getRememberTokenName();
		if (!$isRememberTokenAttribute) {
			parent::setAttribute($key,$value);
		}
	}
}
