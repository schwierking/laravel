@extends('template')
@section('title','Sell Book')
@section('content')
<h2>Sell Your Books!</h2>
@yield('success')
<table>
	<form method="post" action="{{ action('SellController@form') }}">
		<tr>
			<td>
				<label>ISBN: </label>
  			<input type="text" id="isbn" name="isbn" value="{{ old('isbn') }}"/>
			</td>
			<td class="error">
				@if ($errors->has('isbn'))
					{{ $errors->first('isbn') }}
				@endif
			</td>
		</tr>
		<tr>
			<td>
				<label>Condition: </label>
				<select id="condition" name="condition">
    			<option value="1"{{ old('condition')=='1' ?'selected="selected"':'' }}>Poor</option>
    			<option value="2"{{ old('condition')=='2' ? 'selected="selected"':'' }}>Fair</option>
    			<option value="3"{{ old('condition')=='3' ? 'selected="selected"':'' }}>Good</option>
    			<option value="4"{{ old('condition')=='4' ? 'selected="selected"':'' }}>Excellent</option>
  			</select>
			</td>
		</tr>
		<tr>
  		<td>
				<label>Price: </label>
  			<input type="text" id="price" name="price" value="{{ old('price') }}"/>
			</td>
			<td class="error">
				@if ($errors->has('price'))
					{{ $errors->first('price') }}
				@endif	
			</td>
		</tr>
		<tr>
			<td>
				<textarea rows="4" cols="50" maxlength="300" name="comments" placeholder="Comments (optional)">{{ old('comments') }}</textarea>
			</td>
		</tr>
		<tr>
			<td>
				<input type="submit" value="Done"/>
			</td>
		</tr>
		@csrf
	</form>
</table>
@endsection
