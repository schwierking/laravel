@extends('template')
@section('title','Laravel HW06')
@section('content')
	<h2>List of Publishers</h2>
	<pre>
	<table>
		<tr>
				<td><h3>Title</h3></td>
				<td><h3>Authors</h3></td>
				<td><h3>Condition</h3></td>
				<td><h3>Price</h3></td>
		</tr>
		@foreach ($lists as $info)
			<tr class="tr" id="<?php print_r($info->listing_id) ?>">
				<td>
					<?php print_r($info->title); ?>
				</td>
				<td>
					<?php print_r($info->authors); ?>
				</td>
				<td style="text-align: center">
					<?php print_r($info->condition); ?>
				</td>
				<td>
					<?php print_r($info->price); ?>
				</td>
				<div class="popup"><span class="text" id="popup_<?php print_r($info->listing_id) ?>"></span></div>
			</tr>
		@endforeach
	</table>
	</pre>
@endsection
@section('jquery')
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script>
		$(document).ready(function() {
			$(".tr").click(function(event) {
				listing_id = this.id;
				$.post("hello",{'_token':$('meta[name="csrf-token"]').attr("content"), listing_id: listing_id},function(data) {
					var obj = JSON.parse(data);
					obj = obj[0];

					id = "popup_"+listing_id;
					var txt = document.getElementById(id);
					txt.innerHTML = " Title: "+obj.title+"<br> Authors: "+obj.authors+"<br> Publisher: "+obj.publisher+"<br> Year: "+obj.year+"<br> Notes: "+obj.seller_notes+"<br> Price: "+obj.price+"<br> Conditon: "+obj.condition+"<br> Email: "+obj.email;
					txt.classList.toggle("show");
				});
			});
		});
	</script>
@endsection
