<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>@yield('title')</title>

		<link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
	
		<style>
			html, body {
				background-color: cadetblue;
				color: #636b6f;
				font-family: 'Rubik', sans-serif;
				font-weight: 600;
				height: 100vh;
				margin: 0;
			}

			.full-height { height: 100vh; }

			.flex-center {
				align-items: center;
				display: flex;
				justify-content: center;
			}

			.position-ref { position: relative; }

			.top-right {
				position: absolute;
				right: 10px;
				top: 18px;
			}

			.content { text-align: center; }

			.title { font-size: 84px; }

			.links > a {
				color: #636b6f;
				padding: 0 25px;
				font-size: 12px;
				font-weight: 600;
				letter-spacing: .1rem;
				text-decoration: none;
				text-transform: uppercase;
			}
			table {
				margin: auto;
				font-family: 'Rubik', sans-serif;
			}
			h2, h3 {
				text-align: center;
			}
			table td, h2 {
				background-color: beige;	
				padding: 5px;
			}
			.m-b-md { margin-bottom: 30px; }
			.error { 
				color: red; 
				background-color: cadetblue;
				-webkit-text-stroke: 0.5px black; 
			}
			.tr:hover {
				color: palevioletred;
				cursor: pointer;
			}
			.popup {
				position: relative;
				display: inline-block;
				curser: pointer;
				-webkit-user-select: none;
				-moz-user-select: none;
				-ms-user-select: none;
				user-select: none;
			}
			.popup .text {
				visibility: hidden;
				background-color: #555;
				color: #fff;
				border-radius: 6px;
				padding: 8px;
				position: absolute;
				z-index: 1;
				left: 50%;
				margin-left: 550px;
				margin-top: 40px;
			}
			.popup .text::after {
				position: absolute;
				top: 100%;
				left: 50%;
				margin-left: -5px;
				border-width: 5px;
				border-style: solid;
				border-color: #555 transparent transparent transparent;
			}
			.popup .show {
				visibility: visible;
				-webkit-animation: fadeIn 1s;
				animation: fadeIn 1s;
				width: max-content;
			}
			@-webkit-keyframes fadeIn {
				from { opacity: 0; }
				to { opacity: 1; }
			}
			@keyframes fadeIn {
				from { opacity: 0; }
				to { opacity: 1; }
			}
		</style>
	</head>
	<body>
		<div>
			@yield('content')
		</div>
		@yield('jquery')
	</body>
</html>	
